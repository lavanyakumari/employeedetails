package com.bezkoder.spring.data.mongodb.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "employees")
public class Employees {
  @Id
  private String id;

  private String name;
  private String role;
  private boolean salary;
  private boolean joining;

  ...
}