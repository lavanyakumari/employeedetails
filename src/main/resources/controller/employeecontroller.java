package com.bezkoder.spring.data.mongodb.controller;
...

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class EmployeeController {

  @Autowired
  EmployeeRepository employeeRepository;

  @GetMapping("/employees")
  public ResponseEntity<List<Empolyee>> getAllEmployee(@RequestParam(required = false) String title) {
    
  }

  @GetMapping("/emloyees/{id}")
  public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") String id) {
    
  }

  @PostMapping("/employees")
  public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
    
  }

  @PutMapping("/employees/{id}")
  public ResponseEntity<Employee> updateEmployee(@PathVariable("id") String id, @RequestBody Employee employee) {
    
  }

  @DeleteMapping("/employees/{id}")
  public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") String id) {
    
  }

  @DeleteMapping("/employees")
  public ResponseEntity<HttpStatus> deleteAllEmployees() {
    
  }

  @GetMapping("/employees/published")
  public ResponseEntity<List<Employee>> findByPublished() {
    
  }

}